import socket
import struct
import sys
import math
import time


class Client:
    def __init__(self, ip, port):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect((ip, port))

    def send_file(self, filename):
        # sending name of the file
        name = struct.pack('>I', len(filename)) + filename.encode()
        self._sock.send(name)

        # send length of the file
        with open(filename, 'rb') as file:
            size = len(file.read())
            self._sock.send(struct.pack('>I', size))

        # sending file itself
        with open(filename, 'rb') as file:
            i = 0
            while True:
                if math.floor(i * 1024 / size * 100) % 5 == 0:
                    print(f"sent {math.floor(i * 1024 / size * 100)}% of file", end="\r")
                    time.sleep(0.03)
                chunk = file.read(1024)
                if not chunk:
                    print("File sent (if you didn't see the progress bar, try uploading a larger file :))")
                    break

                self._sock.send(chunk)
                i += 1


if __name__ == "__main__":
    try:
        client = Client(sys.argv[2], int(sys.argv[3]))
        client.send_file(sys.argv[1])
    except IndexError:
        raise Exception("Usage: python client.py <filename> <ip> <port>")
